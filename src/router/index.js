import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Calendar from '../views/Calendar.vue'
import DemoTest from '../views/DemoTest.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/Home',
    name: 'Home',
    component: Home,
    children: [
      {
        path: '/DemoTest',
        name: 'DemoTest',
        component: DemoTest
      }
    ]
  },
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/Calendar',
    name: 'Calendar',
    component: Calendar
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
