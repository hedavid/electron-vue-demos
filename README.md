# electron-vue跨平台桌面应用开发实战教程

[electron-vue跨平台桌面应用开发实战教程（一）——项目搭建](https://my.oschina.net/david1025/blog/3168417)  

[electron-vue跨平台桌面应用开发实战教程（二）——主进程常用配置](https://my.oschina.net/david1025/blog/3168418)  

[electron-vue跨平台桌面应用开发实战教程（三）——动态修改窗口大小](https://my.oschina.net/david1025/blog/3168471)  

[electron-vue跨平台桌面应用开发实战教程（四）——窗口样式&打开新窗口](https://my.oschina.net/david1025/blog/3168886)  

[electron-vue跨平台桌面应用开发实战教程（五）——系统通知&托盘](https://my.oschina.net/david1025/blog/3170792)  

[electron-vue跨平台桌面应用开发实战教程（六）——打包](https://my.oschina.net/david1025/blog/3173819)  

[electron-vue跨平台桌面应用开发实战教程（七）——ffi调用C++（Windows平台）](https://my.oschina.net/david1025/blog/3173842)  

[electron-vue跨平台桌面应用开发实战教程（七）——ffi调用C++（macOS平台）](https://my.oschina.net/david1025/blog/3173897)

[electron-vue跨平台桌面应用开发实战教程（八）——edgejs调用C# dll ](https://my.oschina.net/david1025/blog/3174033)

在运行教程的第七篇和第八篇的时候，需要按照第七篇和第八篇文章的要求，安装node-gyp和windows编译工具才能正常运行

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
